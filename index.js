// use the "Require" directive to load node.js modules
  // A "module" is a software component or part of a program that contains one or more routines.
//"Http module" let's Node.js to transfer data using the Hypertext Transfer Protocol.

    // This is a set of indivual files that contain code to create a "component" that helps esstablish data transfer between application.
    // allows us to fetch resources such as HTML documents.
// clients (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages.(requests/responses).
// the message sent by the client is called "request".
// the message sent by the server as ana answer is called "response".
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to the requests on a specified port and gives responses back to the client.

// A port is a virtual point where network connections start and end.
// Each port is associated with specific process or services.
// The server will be assigned to port 4000 via the "listen()" method where the server will listen to any request that are sent to it and will also sen the response via this port.
http.createServer(function(request, response){
  //use to the writeHead() method to:
    // Set a status code for the response. (200 means "success")
    // Set the content-type of the  response. (plain-text message).
  response.writeHead(200, {"Content-type": "text/plain"});
  response.end("Hello World");
  // response.writeHead(200, {"Content-type": "text/html"});
  // response.end("<h1>Hello World</h1>");
}).listen(4000);

console.log("Server is running at localhost:4000");
// 3000, 4000, 8000, 5000 - Usually used for web development
// we will access the server in the browser using the localhost:4000














//
